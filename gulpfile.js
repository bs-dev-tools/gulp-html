/*!
 * gulp
 * npm install 
 */

// Load plugins
var gulp = require('gulp'),
    bootlint = require('gulp-bootlint'),
    browserSync = require('browser-sync').create(),
    cache = require('gulp-cache'),
    concat = require('gulp-concat'),
    cssnano = require('gulp-cssnano'),
    del = require('del'),
    imagemin = require('gulp-imagemin'),
    jshint = require('gulp-jshint'),
    plumber = require('gulp-plumber'),
    rename = require('gulp-rename'),
    sass = require('gulp-sass'),
    stripDebug = require('gulp-strip-debug'),
    sourcemaps = require('gulp-sourcemaps'),
    uglify = require('gulp-uglify'),
    watch = require('gulp-watch');

// Src file shortcuts
var src = {
  html: 'src/**/*.html',
  scss: 'src/assets/styles/*.scss',
  js: 'src/assets/js/**/*.js',
  jsCustom: ['src/assets/js/**/*.js', '!src/assets/js/vendor/**'],
  jsVendor: 'src/assets/js/vendor/**',
  images: 'src/assets/images/**/*',
  fonts: 'src/assets/fonts/*'
};

// Dist file shortcuts
// -- for when we can't use the base option in gulp.src
var dist = {
  styles: 'dist/assets/styles',
  js: 'dist/assets/js'
};


// Clean
gulp.task('clean', function() {
  return del(['dist']);
});


// Default task
gulp.task('default', ['clean'], function() {
  gulp.start('webserver');
});


//=== DIST ===\\
gulp.task('dist', ['clean'], function() {
  gulp.start('markup', 'fonts', 'styles', 'scripts-custom-dist', 'scripts-vendor', 'images');
});


// Fonts
gulp.task('fonts', function() {
  return gulp.src(src.fonts, { base: 'src' })
    .pipe(gulp.dest('dist'));
});


// Images
gulp.task('images', function() {
  return gulp.src(src.images, { base: 'src' })
    .pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
    .pipe(gulp.dest('dist'));
});


// Markup
gulp.task('markup', function() {
  return gulp.src(src.html, { base: 'src' })
    .pipe(bootlint())		// Check if valid bootstrap
    .pipe(gulp.dest('dist'));
});


// Scripts 
gulp.task('scripts-custom', function() {
  return gulp.src(src.jsCustom)
    .pipe(plumber())
    .pipe(jshint('.jshintrc'))
    .pipe(jshint.reporter('default'))
    .pipe(concat('main.js'))
    .pipe(rename({ suffix: '.min' }))
    .pipe(uglify())
    .pipe(gulp.dest(dist.js));
});


gulp.task('scripts-custom-dist', function() {
  return gulp.src(src.jsCustom)
    .pipe(concat('main.js'))
    .pipe(rename({ suffix: '.min' }))
    .pipe(stripDebug())
    .pipe(uglify())
    .pipe(gulp.dest(dist.js));
});


gulp.task('scripts-vendor', function() {
  return gulp.src(src.jsVendor, { base: 'src' })
    .pipe(gulp.dest('dist'));
});


// Styles
// Does not concat files (to maintain mobile/desktop separation)
gulp.task('styles', function() {
  return gulp.src(src.scss)
    .pipe(plumber())
    .pipe(concat('main.css'))
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(rename({ suffix: '.min' }))
    .pipe(cssnano({
      autoprefixer: {
        add: true,
        browsers: ['last 2 versions']
      }
    }))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(dist.styles));
});


// Watch
gulp.task('watch',
    ['fonts', 'images', 'markup', 'styles', 'scripts-custom', 'scripts-vendor'],
    function() {

  // Watch .html files
  watch(src.html, function() {
    gulp.start('bs-inject-html');
  });

  // Watch .scss files
  watch(src.scss, function() {
    gulp.start('styles');
  });

  // Watch .js files
  watch([src.js, '!' + src.jsVendor], function() {
    gulp.start('bs-reload');
  });

  watch(src.jsVendor, function() {
    gulp.start('scripts-vendor');
  });

  // Watch image files
  watch(src.images, function() {
    gulp.start('images');
  });
});


// Webserver
gulp.task('webserver', ['watch'], function() {
  browserSync.init({
    server: 'dist',
    files: [
      '**/*.css',
      '**/images/**'
    ],
    logLevel: 'debug'
  });
});

// Browser-sync tasks
gulp.task('bs-inject-html', ['markup'], function(done) {
  browserSync.reload('**/*.html');
  done();
});

gulp.task('bs-reload', ['scripts-custom'], function(done) {
  browserSync.reload('**/*.js');
  done();
});
