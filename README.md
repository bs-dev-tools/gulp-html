This Gulp process manages the development of HTML sites/apps/whatever. 

The default task cleans the 'dist/' directory and adds the .htaccess file
(if there is one) to the directory. Afterwards, it kicks off processing
HTML, Scss, and JS files. Gulp fires up a server to host the files from 
the 'dist/' directory using BrowserSync to handle automatic updates to 
the browser.

-HTML files-
HTML files are first validated using the W3C validator and then using the
Bottlint. Then they are copied to the distribution folder. BrowserSync then
reloads the browser to display changes.

-Scss files-
Scss files are concatenated, processed into CSS, and then minified.
Sourcemaps are created for easier debugging. BrowserSync injects the 
minified CSS once the process is done.

-JS files-
JS related tasks are split into two categories: custom and vendor scripts.
Custom scripts are any JS files located in the 'src/assets/js' directory.
Any custom scripts are run through a linter (JSHint), concatenated, and 
uglified. BrowserSync reloads the page afterwards to inject the new JS.
Vendor scripts are located in the 'src/assets/js/vendor/' direcotry.
Vendor scripts are simply copied into the 'dist/assets/js/vendor' directory.

-Images-
Images are compressed and sent to the 'dist/assets/images' directory.
BrowserSync injects the images in on completion.

-Watch tasks-
All PHP, Scss, JS, and image files are watched.
